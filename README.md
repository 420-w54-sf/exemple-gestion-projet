# Exemple Gestion Projet

Projet pour présenter les outils de gestion de projet et de flux de travail avec GitLab.

## Tâches (issues)

Les tâches peuvent être créées et modifiées en allant dans le menu 📆 Plan > Issues

Une bonne tâche explique sommairement ce qui doit être réalisé, la liste des tâches détaillées ainsi que les informations complémentaires pouvant aider la réalisation.

### Exemple

voir [Créer la recette de Guacamole](https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/issues/3)

Pour faire un suivi adéquat des tâches, il est nécessaire qu'elle soit assignée à un jalon (Milestone). Lorsqu'une tâche est en cours, elle doit être assignée à une personne.

### Gabarits
 
Vous pouvez créer des gabarits pour les tâches, il suffit de les placer dans le dossier `.gitlab/issues_templates/modèle.md`

Si vous nommez le fichier `default.md` il sera appliqué par défaut

## Jalons (milestone)

Toutes les tâches créées devraient être assignées à un jalon. De cette façon, il y n'y a pas de tâche flottante qui seront perdues dans la liste de tâche. Il faut se questionner si un tâche n'est pas attachée à un jalon et la rattacher au jalon le plus approprié.

[Voir les jalons du projet](https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/milestones)

### Jalon d'itération

Le jalon d'itération est utilisé pour identifier les tâches en cours ou qui seront réalisées prochainement. Il n'est pas rare de travailler avec 2 jalons d'itération. Un pour l'itération courante et un pour l'itération suivante.

[Jalon d'itération](https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/milestones/1)

### Jalon de backlog

Le jalon de backlog est utilisé pour emmagasiner les tâches hors itération, mais qui doivent être réalisées dans une itération à venir. Idéalement les tâches du backlog sont priorisées. La priorisation des tâches peut se faire à l'aide des dates d'échéance (Due date) ou des étiquettes.

## Étiquettes (labels)

Les étiquettes pour le projet peuvent être créées et modifiées en allant dans le menu 👨‍👦 Manage > Labels (https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/labels)

Les étiquettes peuvent avoir plusieurs utilités.

### Création d'un tableau Kanban

La présentation des tâches dans l'interface des jalons de Gitlab est très limité. Si on veut avoir plus de catégories pour effectuer un meilleur suivi des tâches, on peut combiner les étiquettes avec le tableau de tâche (Issue Boards)

https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/boards

En combinant les catégories ouvert / fermé aux nouvelles étapes de flux des tâches, vous pouvez faire un suivi précis de votre projet

#### Création des étiquettes

Créez une étiquette pour chaque colonne et donnez-lui un préfixe (ex: IB:, Flux:, etc.)

Voici une liste de colonnes, mais vous pourriez en ajouter plus au besoin

* ~"IB:À faire"
* ~"IB:En cours"
* ~"IB:En revue"
* ~"IB:Terminé"

Dans le tableau de tâches, il vous reste ensuite à créer vos colonnes et les placer dans l'ordre souhaité

### Priorisation de tâche

Pour la priorisation des tâches, on peut créer des étiquettes. Il suffit ensuite de créer un code de priorisation

Voici une liste de priorité, mais vous pourriez travailler différemment

* ~"p1:urgent"
* ~"p2:haute"
* ~"p3:normale"
* ~"p4:faible"

#### Légende
~"p1:urgent"
Il s'agit de la priorité la plus haute, un bug ou un bloquant impactant toute l'équipe ou rendant le produit inutilisable..

~"p2:haute"
Il s'agit d'une tâche ayant une priorité élevée. un bug ou un bloquant impactant une grande partie de l'équipe ou du produit.

~"p3:normale"
Il s'agit d'une tâche faisant partie de flux normal. Ni bloquante ni problématique.

~"p4:faible"
Il s'agit d'une tâche dont on ne veut pas oublier l'existence, mais qui n'est pas prioritaire du tout.

#### Tâche non priorisée

Si une tâche ne peut pas se classer selon l'échelle de priorité, c'est quelle est peu importante. Le bon réflexe est de fermer la tâche.

#### Abonnement

Il est possible de s'abonner à une étiquette. À ce moment-là, dès qu'une tâche sera assignée de cette étiquette vous recevrez une notification. Intéressant pour les étiquettes de priorité 1 ou 2.

## Merge request

Les Merges Requests sont un excellent outil pour partager la connaissance sur le développement et pour vérifier que toutes les tâches ont été accomplies.

[Exemple de Merge request](https://gitlab.com/420-w54-sf/exemple-gestion-projet/-/merge_requests/1)

### Préparer une revue de code

Il s'agit d'une bonne pratique de documenter une revue de code, de lier les tâches incluses (idéalement 1 seule par MR). 

Le développeur peut également faire une revue de son propre code et le commenter pour attirer l'attention des réviseurs sur des éléments importants de la revue.